import { MasterMindComponent } from './master-mind.component';

export class TestingClass {

    constructor(private testElement: MasterMindComponent) {

    }

    nullTester() {
        let segreto = "0123";
        let tentativo = null;
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    notNumberTest() {
        let segreto = "0123";
        let tentativo = "abcd";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    troppoCortoTest() {
        let segreto = "1234";
        let tentativo = "123";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    troppoLungoTest() {
        let segreto = "1234";
        let tentativo = "12345";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    trimmatoUnoTest() {
        let segreto = "1234";
        let tentativo = "1234   ";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    trimmatoDueTest() {
        let segreto = "1234";
        let tentativo = "   1234";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    depuntatoTest() {
        let segreto = "1234";
        let tentativo = "1.234";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    zeroPosCorretteTest() {
        let segreto = "1234";
        let tentativo = "9876";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    unaPosCorretteTest() {
        let segreto = "1234";
        let tentativo = "1876";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    duePosCorretteTest() {
        let segreto = "1234";
        let tentativo = "1836";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    trePosCorretteTest() {
        let segreto = "1234";
        let tentativo = "1534";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    tentativoCorrettoTest() {
        let segreto = "1234";
        let tentativo = "1234";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    unaPosErrataTest() {
        let segreto = "1234";
        let tentativo = "1300";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    quattroPosErrataTest() {
        let segreto = "1234";
        let tentativo = "4321";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    duePosErrataConDuplicatiTest() {
        let segreto = "1231";
        let tentativo = "4120";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    unaPosErrataConDuplicatiTest() {
        let segreto = "1010";
        let tentativo = "4123";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    unaPosErrataConUnaPosCorrettaATest() {
        let segreto = "1234";
        let tentativo = "1456";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }


    unaPosErrataConUnaPosCorrettaBTest() {
        let segreto = "1234";
        let tentativo = "1456";
        console.log(this.testElement.elaboraNumero(segreto, tentativo));
    }



}