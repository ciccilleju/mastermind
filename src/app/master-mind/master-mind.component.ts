import { Component, OnInit, EventEmitter } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { ParametriService } from '../parametri.service';
import { TestingClass } from './master-mind-test.component';


@Component({
  selector: 'app-master-mind',
  templateUrl: './master-mind.component.html',
  styleUrls: ['./master-mind.component.css'],

})


export class MasterMindComponent implements OnInit {


  storicoIsChanged = new EventEmitter<number[]>();
  cifreOkIsChanged = new EventEmitter<number[]>();
  cifreKoIsChanged = new EventEmitter<number[]>();

  cifreOk: number[] = [];
  cifreKo: number[] = [];
  numeroTentativi: number;


  numeroGenerato: string = "";
  numeroVincente: string = "";
  storicoNrDigitati: number[] = [];
  numeroDigitato: any;

  cifreGenerate: any = [];
  cifreGenerateTemp: any = [];
  cifreDigitate: any = [];

  isNumeroIndovinato: boolean = false;

  cifrePostoGiusto: number = 0;
  cifrePostoSbagliato: number = 0;

  masterMindForm: FormGroup;

  constructor(private parametriService: ParametriService) {


  }

  ngOnInit() {

    this.generaNumero();

    this.numeroVincente = this.numeroGenerato;
    this.masterMindForm = new FormGroup({
      numeroDigitatoForm: new FormControl('', [Validators.required, Validators.pattern('[0-9 ]*'), Validators.minLength(this.parametriService.nCifre), Validators.maxLength(this.parametriService.nCifre)])
    });

    this.storicoIsChanged
      .subscribe(
        (numbers: number[]) => {
          this.storicoNrDigitati = numbers;
        }
      );


    this.cifreOkIsChanged
      .subscribe(
        (numbers: number[]) => {
          this.cifreOk = numbers;
        }
      );

    this.cifreKoIsChanged
      .subscribe(
        (numbers: number[]) => {
          this.cifreKo = numbers;
        }
      );

  }


  generaNumero() {

    for (let i = 0; i < this.parametriService.nCifre; i++) {

      this.numeroGenerato += Math.floor(Math.random() * 10) + "";
    }


    for (let i = 0; i < this.parametriService.nCifre; i++) {
      this.cifreGenerate.push(this.numeroGenerato.substring(i, i + 1));
    }
    console.log(this.numeroGenerato);
  }


  elaboraNumero(numeroDigitato, numeroGenerato) {

    /* PARTE RELATIVA AI TEST 

    let presenteErrore = this.controllaErrori(numeroDigitato, numeroGenerato);

    if (presenteErrore != '') {
      return presenteErrore;
    }

     FINE PARTE RELATIVA AI TEST */

    this.addNrToStorico(numeroDigitato.value);
    this.cifreGenerateTemp = this.cifreGenerate.slice();
    this.isNumeroIndovinato = false;

    this.cifrePostoGiusto = 0;
    this.cifrePostoSbagliato = 0;

    this.numeroDigitato = numeroDigitato.value;

    if (this.numeroDigitato === numeroGenerato) {
      this.isNumeroIndovinato = true;
      this.numeroVincente = numeroGenerato;

    } else {

      for (let i = 0; i < this.parametriService.nCifre; i++) {
        this.cifreDigitate.push(this.numeroDigitato.substring(i, i + 1));
      }

      for (let i = 0; i < this.parametriService.nCifre; i++) {
        if (this.cifreDigitate[i] == this.cifreGenerateTemp[i]) {
          this.cifrePostoGiusto++;
          this.cifreGenerateTemp[i] = 'X';
        }
      }


      for (let i = 0; i < this.parametriService.nCifre; i++) {

        let indice = 0;
        while (indice < this.parametriService.nCifre) {
          if (this.cifreDigitate[i] === this.cifreGenerateTemp[indice]) {
            this.cifrePostoSbagliato++;
            indice = this.parametriService.nCifre;
          } else {
            indice++;
          }
        }
      }
    }

    this.cifreOk.push(this.cifrePostoGiusto);
    this.cifreKo.push(this.cifrePostoSbagliato);
    this.cifreOkIsChanged.emit(this.cifreOk.slice());
    this.cifreKoIsChanged.emit(this.cifreKo.slice());
    this.numeroTentativi = this.storicoNrDigitati.length;
    this.cifreDigitate = [];
  }

  addNrToStorico(number: number) {
    this.storicoNrDigitati.push(number);


    this.storicoIsChanged.emit(this.storicoNrDigitati.slice());


  }

  /* controllaErrori(digitato, generato) {
    let errore = "";

    if (digitato == '') {
      errore += "Numero digitato nullo ";
    }

    if (generato == '') {
      errore += "Numero generato nullo ";
    }

    if (digitato.length > generato.length) {
      errore += "Lunghezza numero generato/digitato diversa: numero digitato troppo lungo ";
    }

    if (digitato.length < generato.length) {
      errore += "Lunghezza numero generato/digitato diversa: numero digitato troppo corto ";
    }

    return errore;
  } */


  giocaAncora() {
    this.numeroVincente = this.numeroGenerato;
    this.resetFields();
    this.parametriService.isGameStarted = false;
  }

  arrenditi() {
    this.numeroVincente = this.numeroGenerato;
    this.resetFields();
  }

  resetFields() {
    this.masterMindForm.reset();
    this.cifreGenerate = [];
    this.cifreDigitate = [];
    this.numeroDigitato = "";
    this.numeroGenerato = "";
  }
}
