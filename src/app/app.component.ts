import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { ParametriService } from './parametri.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ParametriService]
})
export class AppComponent implements OnInit {


  nCifreGenerateForm: FormGroup;

  constructor (private parametriService: ParametriService) {

  }

  ngOnInit() {

    this.nCifreGenerateForm = new FormGroup({
      nCifreForm: new FormControl('', [Validators.required, Validators.pattern('[1-9 ]*'), Validators.minLength(1), Validators.maxLength(1)])
    });


  }
  contaQuanteCifre(nCifre) {

    this.parametriService.nCifre = nCifre.value;

    this.parametriService.isGameStarted = true;
    this.nCifreGenerateForm.reset();
  }

}
