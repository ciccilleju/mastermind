import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Validators, FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MasterMindComponent } from './master-mind/master-mind.component';


@NgModule({
  declarations: [
    AppComponent,
    MasterMindComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
  
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
